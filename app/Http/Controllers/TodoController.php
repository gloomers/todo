<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Todo;
use App\Helpers\Helper;
use DB;
use Response;

class TodoController extends Controller
{
    public function index()
	{
		$listhtml = self::getlisthtml();
		return view('todo.index', compact('listhtml'));
	}
	
	public function getlist()
	{
		try{
			$todo	= new Todo;
			$list	= $todo->getlist();
		}catch(Exception $e){
			return Helper::fail('Failed to load todo list.');
		}
		$data = array(
			'success'	=> true,
			'message'	=> '',
			'html'		=> utf8_encode(view('todo.list', compact('list'))),
			'count'		=> count($list)
		);
		return Response::json($data);
	}
	
	public function getlisthtml()
	{
		$todo = new Todo;
		$list = $todo->getlist();
		return view('todo.list', compact('list'));
	}
	
	public function add(Request $request)
	{	
		try{
			$maxPriority	= Todo::max('priority');
			$maxPriority++;
			
			$data = array(
				'title'		=> $request->title,
				'due'		=> $request->due,
				'priority'	=> $maxPriority
			);
			
			$todo	= Todo::create($data);
		}catch(Exception $e){
			return Helper::fail('Failed to add new item.');
		}
		return Helper::success('Item added successfully.');
	}
	
	public function destroy($todo)
	{
		try{
			$todo->delete();
		}catch(Exception $e){
			return Helper::fail('Failed to delete an item.');
		}
		return Helper::success('Item deleted successfully.');
	}
	
	public function up($id)
	{
		try{
			$todo_up				= Todo::find($id);
			$todo_up_priority		= $todo_up->priority;
			
			$todo_down				= Todo::where('priority', '<', $todo_up_priority)
				->orderBy('priority', 'desc')
				->first();
			$todo_down_priority		= $todo_down->priority;
			
			$todo_up->priority		= 0;
			$todo_up->save();
			
			$todo_down->priority	= $todo_up_priority;
			$todo_down->save();
			
			$todo_up->priority		= $todo_down_priority;
			$todo_up->save();
		}catch(Exception $e){
			return Helper::fail('Failed to change item priority.');
		}
		return Helper::success('');
	}
	
	public function down($id)
	{
		try{
			$todo_down				= Todo::find($id);
			$todo_down_priority		= $todo_down->priority;
			
			$todo_up				= Todo::where('priority', '>', $todo_down_priority)
				->orderBy('priority', 'asc')
				->first();
			$todo_up_priority		= $todo_up->priority;
			
			$todo_down->priority	= 0;
			$todo_down->save();
			
			$todo_up->priority		= $todo_down_priority;
			$todo_up->save();
			
			$todo_down->priority	= $todo_up_priority;
			$todo_down->save();
		}catch(Exception $e){
			return Helper::fail('Failed to change item priority.');
		}
		return Helper::success('');
	}
	
	public function complete($id)
	{
		try{
			$todo = Todo::find($id);
			$todo->complete = true;
			$todo->save();
		}catch(Exception $e){
			return Helper::fail('Failed to complete an item.');
		}
		return Helper::success('Item completed successfully.');
	}
}