<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Todo extends Model
{
	protected $table	= 'todo';
	protected $fillable	= array('title', 'due', 'priority');
	
	public function getlist()
	{
		return Todo::where('complete', '=', false)->orderBy('priority', 'asc')->get();
	}
}
