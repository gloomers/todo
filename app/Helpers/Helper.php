<?php
namespace App\Helpers;

use Response;

class Helper
{
    public static function success($message)
    {
		return self::return_message(true, $message);
    }
	
	public static function fail($message)
	{
		return self::return_message(false, $message);
	}
	
	public static function return_message($success, $message)
	{
		return Response::json(
			array(
				'success' => $success,
				'message' => utf8_encode(
					view(
						'todo.global_message_' . (($success == true) ? 'success' : 'danger'),
						array('message' => $message)
					)
				)
			)
		);
	}
}