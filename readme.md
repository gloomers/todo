# todo demo

## Requirements

* virtualbox: https://www.virtualbox.org/wiki/Downloads
* vagrant: https://www.vagrantup.com/downloads.html
* git: https://git-scm.com/downloads
* composer: https://getcomposer.org/download/
* PHP optional

## Installation on vagrant

1. add to your hosts file:
```
127.0.0.1 homestead.app
```

2. download laravel/homestead box file:
``` 
vagrant box add laravel/homestead
```

3. install, init Homestead:
```
composer global require "laravel/homestead=~2.0"
homestead init
```

4. clone git repo:
```
git clone https://gitlab.com/gloomers/todo.git
```

5. enter project folder:
```
cd todo
```

6. deploy dependencies:
```
composer install
```

7. deploy homestead:
```
homestead make
```

8. Adjust see my Homestead.yaml and .env files. See my Homestead.yaml.example and .env.example

9. fire up virtual machine:
```
homestead up
```

10. create/re-create/seed database:
```
php artisan migrate:refresh --seed
```

11. navigate browser to URL: http://homestead.app:8000

## Tests

Test files are locates in tests folder.

To run tests:
```
phpunit
```

Alternative ways:
```
vendor/bin/phpunit
```

Alternative if you don't have PHP and/or it is not suitable version
```
homestead ssh
cd todo && phpunit
```

If you have xdebug, with too low max_nesting, increase it in .ini file, for example: xdebug.max_nesting_level=500

## Stop/clean virtual machine:

```
homestead halt
homestead destroy
```

## Manual installation

1. Clone: git clone https://gitlab.com/gloomers/todo.git
2. Install dependencies: composer install
3. Update app configs (url): config/app.php
4. Update configs: config/database.php
5. Initialize database/data: php artisan migrate:refresh --seed