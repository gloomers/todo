<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TodoTest extends TestCase
{
    public function testTodoAdd()
    {
        $uuid   = uniqid();
        $date   = '2000-01-01';
        
        $this->visit('/todo')
            ->type($uuid, '#text-title')
            ->type($date, '#text-due')
            ->see($uuid, $date);
    }
    
    public function testTodoAddJson()
    {
        $this->post('/todo/add', ['title' => uniqid(), 'due' => '2000-01-02'])
            ->seeJson(['success' => true]);
    }
}