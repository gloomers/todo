<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TodoIndexTest extends TestCase
{
    public function testTodoIndex()
    {
        $this->visit('/todo')->see('TODO list');
    }
}
