function reload_todo_list(){
	$.ajax({
		type: 'GET',
		dataType: 'json',
		url: 'todo/getlist',
	}).fail(function(){
		console.log('ERROR');
	}).always(function(returned){
		if (returned.success == true){
			if (returned.count == 0) {
				$('#todo_table').hide();
			}else{
				$('#todo_table tbody').html(returned.html);
				if($('#todo_table').css('display') == 'none' ){
					$('#todo_table').show();
				}
			}
		}else{
			$('#todo_table').hide();
			$('#global-message').html(returned.message);
		}
	});
}

function reset_global_message(){
	$('#global-message').html('');
}

$(document).ready(function(){
	$('.input-group.date').datepicker({
		todayHighlight: true,
		format: "yyyy-mm-dd",
		todayBtn: true
	});

	$.ajaxSetup({
        headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
	});
	
	$('#btn-add').click(function(){
		reset_global_message();
		$.ajax({
			type: 'POST',
			dataType: 'json',
			url: 'todo/add',
			data: {
				title: $('#text-title').val(),
				due: $('#text-due').val()
			}
		}).fail(function(){
			console.log('ERROR');
		}).always(function(returned){
			if(returned.success == true){
				$('#text-title').val('');
				reload_todo_list();
			}
			$('#global-message').html(returned.message);
		});
	});
	
	$('#todo_table').on('click', '.todo-up', function(){
		reset_global_message();
		var id = $(this).attr('data-id');
		$.ajax({
			type: 'GET',
			dataType: 'json',
			url: "todo/up/" + id,
		}).fail(function(){
			console.log('ERROR');
		}).always(function(returned){
			if(returned.success == true){
				reload_todo_list();
			}else{
				$('#global-message').html(returned.message);
			}
		});
	});
	
	$('#todo_table').on('click', '.todo-down', function(){
		reset_global_message();
		var id = $(this).attr('data-id');
		$.ajax({
			type: 'GET',
			dataType: 'json',
			url: "todo/down/" + id,
		}).fail(function(){
			console.log('ERROR');
		}).always(function(returned){
			if(returned.success == true){
				reload_todo_list();
			}else{
				$('#global-message').html(returned.message);
			}
		});
	});
	
	$('#todo_table').on('click', '.todo-complete', function(){
		reset_global_message();
		var id = $(this).attr('data-id');
		$.ajax({
			type: 'GET',
			dataType: 'json',
			url: "todo/complete/" + id,
		}).fail(function(){
			console.log('ERROR');
		}).always(function(returned){
			if(returned.success == true){
				reload_todo_list();
			}
			$('#global-message').html(returned.message);
		});
	});
	
	$('#todo_table').on('click', '.todo-delete', function(){
		reset_global_message();
		var id = $(this).attr('data-id');
		$.ajax({
			dataType: 'json',
			type: 'DELETE',
			url: "todo/" + id
		}).fail(function(){
			console.log('ERROR');
		}).always(function(returned){
			if(returned.success == true){
				reload_todo_list();
			}
			$('#global-message').html(returned.message);
		});
	});
});