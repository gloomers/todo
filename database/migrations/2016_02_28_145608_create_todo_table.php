<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTodoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('todo', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            
            $table->string('title')->default('');
            $table->date('due');
            $table->integer('priority')->unsigned()->default(0);
            
            $table->boolean('complete')->default(false);
            
            $table->index('priority');
            $table->unique('priority');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('todo');
    }
}
