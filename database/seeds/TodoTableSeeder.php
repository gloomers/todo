<?php
 
use Illuminate\Database\Seeder;
 
class TodoTableSeeder extends Seeder{

    public function run()
    {
        DB::table('todo')->delete();
        
        $tododemolist = array(
            ['id' => 1, 'title' => 'Demo title 1', 'complete' => true, 'created_at' => new DateTime, 'updated_at' => new DateTime, 'priority' => 3],
            ['id' => 2, 'title' => 'Demo title 2', 'complete' => false, 'created_at' => new DateTime, 'updated_at' => new DateTime, 'priority' => 2],
            ['id' => 3, 'title' => 'Demo title 3', 'complete' => false, 'created_at' => new DateTime, 'updated_at' => new DateTime, 'priority' => 1]
        );
        
        //// Uncomment the below to run the seeder
        DB::table('todo')->insert($tododemolist);
    }
 
}