@extends('app')
@section('content')
	<div class="page-header">
		<h2>Todo list</h2>
    </div>
	
	<div class="row" id="global-message"></div>

    <div class="row">
		<div class="col-md-13">
			@include('todo.add_table')
		</div>
	</div>
	
	<div class="row">
        <div class="col-md-13">
			@include('todo.todo_table')
        </div>
	</div>
	
	@include('todo.confirm_modal')
@endsection