<table class="table" id="add_table">
<thead>
	<tr>
		<th>&nbsp;</th>
		<th>Title</th>
		<th>Due date</th>
		<th>&nbsp;</th>
	</tr>
</thead>
<tbody>
	<tr>
		<td class="col-md-1">&nbsp;</td>
		<td class="col-md-5"><input type="text" class="form-control" id="text-title" name="title"></td>
		<td class="col-md-2">
			<div class="input-group date">
				<input type="text" id="text-due" class="form-control" value="{{ date('Y-m-d') }}">
				<span class="input-group-addon"><i class="glyphicon glyphicon-th"></i></span>
			</div>
		</td>
		<td><a id="btn-add" href="#" class="btn btn-success btn-md">ADD</a></td>
	</tr>
</tbody>
</table>