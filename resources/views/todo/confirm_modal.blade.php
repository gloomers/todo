<div id="confirm-delete" class="modal">
	<div class="modal-body">Are you sure?</div>
	<div class="modal-footer">
		<button type="button" data-dismiss="modal" class="btn btn-warning" id="delete">DELETE</button>
		<button type="button" data-dismiss="modal" class="btn">CANCEL</button>
	</div>
</div>