<table class="table" id="todo_table">
<thead>
	<tr>
		<th>ID</th>
		<th>Title</th>
		<th>Due date</th>
		<th colspan="2" class="text-center">Priority</th>
		<th class="text-center">Operations</th>
	</tr>
</thead>
<tbody>
	{!! $listhtml !!}
</tbody>
</table>