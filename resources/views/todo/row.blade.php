<tr>
	<td class="col-md-1">{{ $todo->id }}</td>
	<td class="col-md-5">{{ $todo->title }}</td>
	<td class="col-md-2">{{ $todo->due }}</td>
	<td>
		@if( $first != true )
			<a data-id="{{ $todo->id }}" href="#" class="todo-up btn btn-primary btn-xs text-left"><span class="glyphicon glyphicon-menu-up"></span></a>
		@endif
	</td>
	<td>
		@if( $last != true )
			<a data-id="{{ $todo->id }}" href="#" class="todo-down btn btn-primary btn-xs text-right"><span class="glyphicon glyphicon-menu-down"></span></a>
		@endif
	</td>
	<td class="col-md-3">
		<a data-id="{{ $todo->id }}" href="#" class="todo-complete btn btn-success btn-xs">COMPLETE</a>
		<!--<a data-id="{{ $todo->id }}" href="#" class="todo-edit btn btn-primary btn-xs">EDIT</a>-->
		<a data-id="{{ $todo->id }}" href="#" class="todo-delete btn btn-danger btn-xs">DELETE</a>
	</td>
</tr>