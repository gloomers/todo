<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Todo Demo for Exacaster</title>
	<link href="{{ URL::asset('css/bootstrap.min.css') }}" rel="stylesheet">
	<link href="{{ URL::asset('css/bootstrap-theme.min.css') }}" rel="stylesheet">
	<link href="{{ URL::asset('css/bootstrap-datepicker3.standalone.min.css') }}" rel="stylesheet">
	<link href="{{ URL::asset('css/todo.css') }}" rel="stylesheet">
	<script src="{{ URL::asset('js/jquery-2.2.1.min.js') }}"></script>
	<script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
	<script src="{{ URL::asset('js/bootstrap-datepicker.min.js') }}"></script>
	<script src="{{ URL::asset('js/todo.js') }}"></script>
	<meta name="csrf-token" content="{{csrf_token()}}">
</head>
<body>
	<div class="container theme-showcase" role="main">
		<div class="jumbotron">
			@yield('content')
		</div>
	</div>
</body>
</html>