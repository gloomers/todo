<!DOCTYPE html>
<html>
    <head>
        <title>Exacaster TODO demo</title>

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 50px;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
            }

            .container {
                display: table-cell;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <h1>
                <a href="todo">TODO application</a>
            </h1>
            <ul>
                <li>PHP version: {{ phpversion () }}</li>
                <li>jQuery</li>
                <li>HTML5</li>
                <li>MySQL</li>
                <li>AJAX</li>
                <li>Unit tests</li>
            </ul>
        </div>
    </body>
</html>
